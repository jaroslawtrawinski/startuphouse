﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using StartupHouse.Interfaces.Exceptions;
using StartupHouse.Interfaces.Remote;

namespace StartupHouse.Implementation.Remote
{
    /// <summary>
    ///     Service handles connection to remote NBP Api.
    /// </summary>
    public class NbpCurrencyApi : ICurrencyApi
    {
        private const string BaseUrl = "http://api.nbp.pl";
        private const string TemplatePath = "api/exchangerates/rates/a/{symbol}/{date}";

        public async Task<decimal> GetCurrencyValueAsync(string symbol, DateTime day)
        {
            var path = TemplatePath.Replace("{symbol}", symbol)
                .Replace("{date}", day.ToString("yyyy-MM-dd"));

            using (var client = new HttpClient() { BaseAddress = new Uri(BaseUrl) })
            {
                try
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = client.GetAsync(path).Result;
                    var responseString = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        var responseObject = JsonConvert.DeserializeObject<NbpResponse>(responseString);
                        return responseObject.Rates[0].Mid;
                    }
                    else
                    {
                        if (response.StatusCode == HttpStatusCode.NotFound)
                        {
                            return 0M;
                        }
                        else
                        {
                            throw new RemoteApiException($"Remote API returned and HTTP Status Code: {response.StatusCode}");
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new RemoteApiException("An error occured when calling external API", ex);
                }
            }
        }

        private class NbpResponse
        {
            public NbpCurrencyRate[] Rates { get; set; }
        }

        private class NbpCurrencyRate
        {
            public decimal Mid { get; set; }
        }
    }
}