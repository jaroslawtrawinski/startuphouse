﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StartupHouse.Database.Entities.Tables.dbo;
using StartupHouse.Database.Interfaces;
using StartupHouse.Interfaces;
using StartupHouse.Interfaces.DTOs;
using StartupHouse.Interfaces.Exceptions;
using StartupHouse.Interfaces.Remote;

namespace StartupHouse.Implementation
{
    public class CurrencyService : ICurrencyService
    {
        private readonly IRepository<Currency> _currencyRepository;
        private readonly IRepository<CurrencyValue> _currencyValuesRepository;
        private readonly ICurrencyApi _currencyApi;
        private readonly IContextScope _contextScope;

        public CurrencyService(
            IRepository<Currency> currencyRepository,
            IRepository<CurrencyValue> currencyValuesRepository,
            ICurrencyApi currencyApi,
            IContextScope contextScope)
        {
            _currencyRepository = currencyRepository;
            _currencyValuesRepository = currencyValuesRepository;
            _currencyApi = currencyApi;
            _contextScope = contextScope;
        }

        /// <summary>
        ///     Gets list of supported currencies.
        /// </summary>
        /// <returns></returns>
        public async Task<IList<CurrencyDto>> GetCurrenciesAsync()
        {
            var symbols = await _currencyRepository
                .Queryable()
                .OrderBy(x => x.Code)
                .Select(c => new CurrencyDto
                {
                    Id = c.Id,
                    Symbol = c.Code
                })
                .ToListAsync();
            return symbols;
        }

        /// <summary>
        ///     Method gets currency values
        /// </summary>
        /// <param name="symbol">The currency symbol.</param>
        /// <param name="fromDate">From date range.</param>
        /// <param name="toDate">To date range.</param>
        public async Task<CurrencyValuesDto> GetCurrencyDataAsync(string symbol, DateTime fromDate, DateTime toDate)
        {
            if (toDate < fromDate)
                throw new ArgumentException($"Param {nameof(toDate)} can not be lower than {nameof(fromDate)}");

            if (toDate.Subtract(fromDate).Days > 30)
                throw new ArgumentException("Can't get data for more than 30 days range.");

            var currencyDb = _currencyRepository.Get(x => x.Code == symbol);
            if (currencyDb == null)
            {
                throw new EntityNotFoundException($"Currency with symbol: {symbol} was not found.");
            }

            var valuesDb = await _currencyValuesRepository
                .Queryable()
                .Where(cv => cv.CurrencyId == currencyDb.Id && cv.Day.Day > fromDate.Day && cv.Day.Day <= toDate.Day)
                .Select(x => new
                {
                    x.Day,
                    x.Value
                }).ToListAsync();

            var requiredDays = Enumerable.Range(0, 1 + toDate.Subtract(fromDate).Days)
                .Select(offset => fromDate.AddDays(offset))
                .ToArray();

            var result = new CurrencyValuesDto
            {
                Symbol = symbol,
                Data = new Dictionary<DateTime, decimal>()
            };

            foreach (var day in requiredDays)
            {
                var valueDb = valuesDb.FirstOrDefault(x => x.Day == day);
                if (valueDb != null)
                {
                    if (valueDb.Value != 0)
                        result.Data.Add(day, valueDb.Value);
                }
                else
                {
                    var externalValue = await UpdateCurrencyDataAsync(currencyDb.Id, day);
                    if (externalValue != 0)
                        result.Data.Add(day, externalValue);
                }
            }
            return result;
        }

        /// <summary>
        ///     Method calls external service to fetch currency data, update value in db and return this value.
        /// </summary>
        /// <param name="currencyId">The identifier of the currency.</param>
        /// <param name="day"></param>
        /// <returns></returns>
        public async Task<decimal> UpdateCurrencyDataAsync(short currencyId, DateTime day)
        {
            var currencyDb = _currencyRepository.Get(x => x.Id == currencyId);
            if (currencyDb == null)
            {
                throw new EntityNotFoundException(nameof(currencyDb));
            }

            var apiValue = await _currencyApi.GetCurrencyValueAsync(currencyDb.Code, day);
            var currencyValue = _currencyValuesRepository.Get(x => x.CurrencyId == currencyId && x.Day.Date == day.Date);
            if (currencyValue == null)
            {
                currencyValue = new CurrencyValue
                {
                    CurrencyId = currencyId,
                    Day = day.Date
                };
                _currencyValuesRepository.Add(currencyValue);
            }
            currencyValue.Value = apiValue;
            await _contextScope.CommitAsync();
            return apiValue;
        }
    }
}