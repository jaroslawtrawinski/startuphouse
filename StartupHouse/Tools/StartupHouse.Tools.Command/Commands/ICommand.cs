﻿using System.Threading.Tasks;

namespace StartupHouse.Tools.Command.Commands
{
    public interface ICommand
    {
        string Name { get;  }

        string Description { get; }

        Task ExecuteAsync();
    }
}