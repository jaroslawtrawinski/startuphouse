﻿using System;
using System.Threading.Tasks;
using Unity;

namespace StartupHouse.Tools.Command.Commands
{
    public class QuitCommand : ICommand
    {
        private readonly IUnityContainer _container;

        public string Name  => "quit";

        public string Description => "Quit";

        public QuitCommand(IUnityContainer container)
        {
            _container = container;
        }

        public Task ExecuteAsync()
        {
            return Task.CompletedTask;
        }
    }
}