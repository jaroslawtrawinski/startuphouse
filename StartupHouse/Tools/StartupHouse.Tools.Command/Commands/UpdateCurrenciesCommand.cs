﻿using System;
using System.Threading.Tasks;
using StartupHouse.Interfaces;

namespace StartupHouse.Tools.Command.Commands
{
    /// <summary>
    ///     Command updates currency values for current day.
    /// </summary>
    public class UpdateCurrenciesCommand : ICommand
    {
        private readonly ICurrencyService _currencyService;

        public string Name => "updatecurrencies";
        public string Description => "Update currencies with current values";

        public UpdateCurrenciesCommand(ICurrencyService currencyService)
        {
            _currencyService = currencyService;
        }


        public async Task ExecuteAsync()
        {
            var currencies = await _currencyService.GetCurrenciesAsync();
            foreach (var currency in currencies)
            {
                await _currencyService.UpdateCurrencyDataAsync(currency.Id, DateTime.Now.Date);
            }
        }
    }
}