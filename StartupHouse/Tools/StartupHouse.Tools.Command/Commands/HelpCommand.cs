﻿using System;
using System.Threading.Tasks;
using Unity;

namespace StartupHouse.Tools.Command.Commands
{
    public class HelpCommand : ICommand
    {
        private readonly IUnityContainer _container;

        public string Name  => "help";

        public string Description => "Display possible commands";

        public HelpCommand(IUnityContainer container)
        {
            _container = container;
        }

        public Task ExecuteAsync()
        {
            Console.WriteLine("------------------------------------");
            Console.WriteLine("The following commands are supported:");

            var commands = _container.ResolveAll<ICommand>();
            foreach (var command in commands)
            {
                Console.WriteLine($"{command.Name} - {command.Description}");
            }

            Console.WriteLine("------------------------------------");

            return Task.CompletedTask;
            
        }
    }
}