﻿using System;
using System.IO;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using StartupHouse.Database.Interfaces;
using StartupHouse.Database.Repository;
using StartupHouse.Implementation;
using StartupHouse.Implementation.Remote;
using StartupHouse.Interfaces;
using StartupHouse.Interfaces.Remote;
using StartupHouse.Tools.Command.Commands;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace StartupHouse.Tools.Command
{
    class Program
    {
        private static UnityContainer _container;
        private static IConfigurationRoot _config;
        private static string Environment;

        static void Main(string[] args)
        {
            _container = BootstrapUnity();

            try
            {
                if (args.Length < 2 || args[0] != "-e")
                {
                    Console.Write("Please provide -e Environment parameter");
                    return;
                }
                Environment = args[1];
                Console.WriteLine($"Environment:{Environment}");

                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .AddJsonFile($"appsettings.{Environment}.json", true, true);
                _config = builder.Build();

                ICommand nextCommand = null;
                do
                {
                    string commandText;
                    if (nextCommand == null && args.Length > 2)
                        commandText = args[2];
                    else
                        commandText = Console.ReadLine();

                    nextCommand = GetCommand(commandText);
                    if (nextCommand != null)
                        nextCommand.ExecuteAsync().Wait();
                    else
                    {
                        Console.WriteLine($"Command: {commandText} is not supported");
                    }
                }
                while (!(nextCommand is QuitCommand));
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Exception: {ex.Message} {ex}");
                Console.WriteLine($"Inner Exception: {ex.InnerException}");
                Console.ForegroundColor = ConsoleColor.Black;
                throw;
            }
        }

        private static ICommand GetCommand(string cmdText)
        {
            var commands = _container.ResolveAll<ICommand>();
            var cmd = commands.FirstOrDefault(x => x.Name == cmdText);
            return cmd;
        }


        private static UnityContainer BootstrapUnity()
        {
            var container = new UnityContainer();

            container.RegisterType<Context>(new PerResolveLifetimeManager(), new InjectionFactory(c =>
            {
                var connectionString = _config["ConnectionString"];
                var dbContextOptionsBuilder = new DbContextOptionsBuilder<Context>()
                    .UseSqlServer(connectionString);
                return new Context(dbContextOptionsBuilder.Options);
            }));

            container.RegisterType<IContextScope, ContextScope>();
            container.RegisterType(typeof(IRepository<>), typeof(Repository<>));
            container.RegisterType<ICurrencyService, CurrencyService>();
            container.RegisterType<ICurrencyApi, NbpCurrencyApi>();

            #region Register commands

            container.RegisterType<ICommand, UpdateCurrenciesCommand>(nameof(UpdateCurrenciesCommand));
            container.RegisterType<ICommand, HelpCommand>(nameof(HelpCommand));
            container.RegisterType<ICommand, QuitCommand>(nameof(QuitCommand));
            #endregion

            return container;
        }
    }
}