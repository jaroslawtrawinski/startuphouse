﻿using System;

namespace StartupHouse.Interfaces.Exceptions
{
    public class RemoteApiException : Exception
    {
        public RemoteApiException() : base()
        {
        }

        public RemoteApiException(string message) : base(message)
        {
        }

        public RemoteApiException(string message, Exception ex) : base(message, ex)
        {
        }
    }
}
