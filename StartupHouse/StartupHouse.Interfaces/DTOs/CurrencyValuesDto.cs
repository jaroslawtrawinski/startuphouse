﻿using System;
using System.Collections.Generic;

namespace StartupHouse.Interfaces.DTOs
{
    /// <summary>
    ///     Class contains currency data.
    /// </summary>
    public class CurrencyValuesDto
    {
        public string Symbol { get; set; }
        
        public Dictionary<DateTime, decimal> Data { get; set; }
    }
}