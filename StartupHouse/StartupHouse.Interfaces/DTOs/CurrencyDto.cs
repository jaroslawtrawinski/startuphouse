﻿namespace StartupHouse.Interfaces.DTOs
{
    public class CurrencyDto
    {
        public short Id { get; set; }

        public string Symbol { get; set; }
    }
}
