﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StartupHouse.Interfaces.DTOs;

namespace StartupHouse.Interfaces
{
    /// <summary>
    ///     Interface handles tasks related to currencies.
    /// </summary>
    public interface ICurrencyService
    {
        /// <summary>
        ///     Gets list of supported currencies.
        /// </summary>
        /// <returns></returns>
        Task<IList<CurrencyDto>> GetCurrenciesAsync();

        /// <summary>
        ///     Get currency data.
        /// </summary>
        /// <param name="symbol">The symbol of the currency.</param>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        Task<CurrencyValuesDto> GetCurrencyDataAsync(string symbol, DateTime fromDate, DateTime toDate);

        /// <summary>
        ///     Method calls remote API to fetch data and updates it in DB.
        /// </summary>
        /// <param name="currencyId">The identifier of the currency.</param>
        /// <param name="day"></param>
        /// <returns></returns>
        Task<decimal> UpdateCurrencyDataAsync(short currencyId, DateTime day);
    }
}