﻿using System;
using System.Threading.Tasks;

namespace StartupHouse.Interfaces.Remote
{
    /// <summary>
    ///     Service handles connection to remote API
    /// </summary>
    public interface ICurrencyApi
    {
        /// <summary>
        ///     Method calls Api and gets currency data.
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="day"></param>
        /// <returns></returns>
        Task<decimal> GetCurrencyValueAsync(string symbol, DateTime day);
    }
}