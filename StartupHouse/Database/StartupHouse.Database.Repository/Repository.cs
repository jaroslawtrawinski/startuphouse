﻿using System;
using System.Linq;
using System.Linq.Expressions;
using StartupHouse.Database.Interfaces;

namespace StartupHouse.Database.Repository
{
    /// <summary>
    ///     Repository 
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : IEntity
    {
        private readonly Context _context;

        /// <summary>
        ///     Initialize the instance.
        /// </summary>
        protected Repository()
        {
        }

        /// <summary>
        ///     Initialize the instance.
        /// </summary>
        /// <param name="context"></param>
        public Repository(Context context)
        {
            _context = context;
        }

        public virtual IQueryable<TEntity> Query => _context.Set<TEntity>();

        public virtual IQueryable<TEntity> Queryable()
        {
            return Query;
        }

        /// <summary>
        ///     Gets single entity
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public virtual TEntity Get(Expression<Func<TEntity, bool>> predicate)
        {
            return Query.FirstOrDefault(predicate);
        }

        /// <summary>
        ///     Adds entity to repository.
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Add(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            _context.Add(entity);
        }

        /// <summary>
        ///     Updates entity in repository.
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Update(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            _context.Update(entity);
        }

        /// <summary>
        ///     Deletes entity from repository.
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Delete(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            _context.Remove(entity);
        }
    }
}