﻿using Microsoft.EntityFrameworkCore;
using StartupHouse.Database.Entities.Tables.dbo;

namespace StartupHouse.Database.Repository
{
    public class Context : DbContext
    {
        public Context() : base()
        {
        }

        public Context(DbContextOptions<Context> options)
            : base(options)
        {
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //if (!optionsBuilder.IsConfigured)
            //{
            //    var connectionString = "Server=(local);Database=StartupHouse.Development;Trusted_Connection=True;";
            //    optionsBuilder.UseSqlServer(connectionString, providerOptions => providerOptions.CommandTimeout(60));
            //}
            base.OnConfiguring(optionsBuilder);
        }

        public virtual DbSet<Currency> Currencies { get; set; }
        public virtual DbSet<CurrencyValue> CurrencyValues { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Currency>()
                .HasMany(e => e.CurrencyValues)
                .WithOne(e => e.Currency)
                .HasForeignKey(k => k.CurrencyId)
                .OnDelete(DeleteBehavior.Restrict);

            #region Composite Keys

            modelBuilder.Entity<CurrencyValue>()
                .HasKey(c => new { c.CurrencyId, c.Day });

            #endregion

            DataSeed(modelBuilder);
        }

        private void DataSeed(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Currency>()
                .HasData(new Currency()
                    {
                        Id = 1,
                        Code = "USD"
                    },
                    new Currency()
                    {
                        Id = 2,
                        Code = "EUR"
                    });
        }
    }
}