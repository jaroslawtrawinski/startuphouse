﻿using System.Threading.Tasks;
using StartupHouse.Database.Interfaces;

namespace StartupHouse.Database.Repository
{
    public class ContextScope : IContextScope
    {
        private readonly Context _context;

        public ContextScope(Context context)
        {
            _context = context;
        }

        public async Task CommitAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}