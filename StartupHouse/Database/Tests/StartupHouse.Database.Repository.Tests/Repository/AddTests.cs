﻿using System;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using StartupHouse.Database.Repository.Tests.SampleEntities;

namespace StartupHouse.Database.Repository.Tests.Repository
{
    [TestFixture]
    public class AddTests
    {
        protected Mock<Context> ContextMock;
        protected Repository<SampleEntity> RepositoryUnderTest;

        [SetUp]
        public void SetUp()
        {
            ContextMock = new Mock<Context>();
            RepositoryUnderTest = new Repository<SampleEntity>(ContextMock.Object);
        }


        [Test]
        public void Add_WhenMethodIsCalled_EntityShouldBeAddedToContext()
        {
            //arrange
            var entityToAdd = new SampleEntity()
            {
                Id = Guid.NewGuid(),
                IntegerProperty = 123,
                StringProperty = "text"
            };
            SampleEntity callBackObject = null;

            ContextMock.Setup(x => x.Add(It.IsAny<SampleEntity>()))
                .Callback<SampleEntity>(addedEntity =>
                {
                    callBackObject = addedEntity;
                });

            //act
            RepositoryUnderTest.Add(entityToAdd);
            //Assert
            entityToAdd.Should().BeEquivalentTo(callBackObject);
            ContextMock.Verify(x => x.Add(It.IsAny<SampleEntity>()), Times.Once);
        }

        [Test]
        public void Add_WhenNullArgumentWasPassed_ShouldThrowArgumentNullException()
        {
            //arrange
            SampleEntity entityToAdd = null;
            //act
            Action act = () => RepositoryUnderTest.Add(entityToAdd);
            //assert
            act.Should().Throw<ArgumentNullException>()
                 .WithMessage("Value cannot be null. (Parameter 'entity')");
        }
    }
}
