﻿using System;
using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using NUnit.Framework;
using StartupHouse.Database.Interfaces;
using StartupHouse.Database.Repository.Tests.Fake;
using StartupHouse.Database.Repository.Tests.SampleEntities;

namespace StartupHouse.Database.Repository.Tests.Repository
{
    [TestFixture]
    public class FakeRepositoryTestBase
    {
        protected IRepository<SampleEntity> RepositoryUnderTests;
        protected IList<SampleEntity> SampleEntities;

        [SetUp]
        public void SetUp()
        {
            SampleEntities = Builder<SampleEntity>
                .CreateListOfSize(10)
                .All()
                    .With(x=>x.Id = Guid.NewGuid())
                .Build()
                .ToList();
            RepositoryUnderTests = new FakeRepository<SampleEntity>(SampleEntities);
        }
    }
}