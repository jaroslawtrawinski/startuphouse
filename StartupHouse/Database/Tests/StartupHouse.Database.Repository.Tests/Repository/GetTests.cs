﻿using System;
using System.Linq;
using NUnit.Framework;

namespace StartupHouse.Database.Repository.Tests.Repository
{
    [TestFixture]
    public class GetTests : FakeRepositoryTestBase
    {
        [Test]
        public void Get_WhenObjectExist_ShouldBeReturned()
        {
            //arrange
            var existingObjectId = SampleEntities.First().Id;
            //act
            var entity = RepositoryUnderTests.Get(x => x.Id == existingObjectId);
            //assert
            Assert.IsNotNull(entity);
        }

        [Test]
        public void Get_WhenObjectDoesNotExist_ShouldNullBeReturned()
        {
            //arrange
            var notExistingObjectId = Guid.Empty;
            //act
            var entity = RepositoryUnderTests.Get(x => x.Id == notExistingObjectId);
            //assert
            Assert.IsNull(entity);
        }
    }
}