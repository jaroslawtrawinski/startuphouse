﻿using System;
using StartupHouse.Database.Interfaces;

namespace StartupHouse.Database.Repository.Tests.SampleEntities
{
    /// <summary>
    ///     Sample entity class
    /// </summary>
    public class SampleEntity : IEntity
    {
        public Guid Id { get; set; }

        public string StringProperty { get; set; }

        public int IntegerProperty { get; set; }
    }
}