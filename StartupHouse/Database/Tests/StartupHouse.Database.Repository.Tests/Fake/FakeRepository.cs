﻿using System.Collections.Generic;
using System.Linq;
using StartupHouse.Database.Interfaces;

namespace StartupHouse.Database.Repository.Tests.Fake
{
    /// <summary>
    ///     Fake repository for testing purposes.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class FakeRepository<TEntity> : Repository<TEntity>  where TEntity : IEntity, new()
    {
        private readonly IList<TEntity> _baseList;

        /// <summary>
        ///     Initialize the instance.
        /// </summary>
        /// <param name="baseList"></param>
        public FakeRepository(IList<TEntity> baseList)
        {
            _baseList = baseList;
        }

        public override IQueryable<TEntity> Query => _baseList.AsQueryable();
    }
}