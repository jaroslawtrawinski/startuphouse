﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using StartupHouse.Database.Interfaces;

namespace StartupHouse.Database.Entities.Tables.dbo
{
    [Serializable, Table("Currency")]
    public class Currency : IEntity
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public short Id { get; set; }

        public string Code { get; set; }

        public virtual ICollection<CurrencyValue> CurrencyValues { get; set; }
    }
}