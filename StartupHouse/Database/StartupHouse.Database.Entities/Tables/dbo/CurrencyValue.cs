﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using StartupHouse.Database.Interfaces;

namespace StartupHouse.Database.Entities.Tables.dbo
{
    [Serializable, Table("CurrencyValue")]
    public class CurrencyValue : IEntity

    {
        public short CurrencyId { get; set; }

        [Column(TypeName = "Date")]
        public DateTime Day { get; set; }

        public decimal Value { get; set; }

        public virtual Currency Currency { get; set; }
    }
}