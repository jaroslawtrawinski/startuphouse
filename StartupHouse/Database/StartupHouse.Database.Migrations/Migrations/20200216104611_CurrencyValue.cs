﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace StartupHouse.Database.Migrations.Migrations
{
    public partial class CurrencyValue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Currency",
                table: "Currency");

            migrationBuilder.AlterColumn<string>(
                name: "Code",
                table: "Currency",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<short>(
                name: "Id",
                table: "Currency",
                nullable: false,
                defaultValue: (short)0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Currency",
                table: "Currency",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "CurrencyValue",
                columns: table => new
                {
                    CurrencyId = table.Column<short>(nullable: false),
                    Day = table.Column<DateTime>(type: "Date", nullable: false),
                    Value = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurrencyValue", x => new { x.CurrencyId, x.Day });
                    table.ForeignKey(
                        name: "FK_CurrencyValue_Currency_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "Currency",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Currency",
                columns: new[] { "Id", "Code" },
                values: new object[] { (short)1, "USD" });

            migrationBuilder.InsertData(
                table: "Currency",
                columns: new[] { "Id", "Code" },
                values: new object[] { (short)2, "EUR" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CurrencyValue");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Currency",
                table: "Currency");

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: (short)1);

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: (short)2);

            migrationBuilder.DropColumn(
                name: "Id",
                table: "Currency");

            migrationBuilder.AlterColumn<string>(
                name: "Code",
                table: "Currency",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Currency",
                table: "Currency",
                column: "Code");
        }
    }
}
