﻿using System;
using System.IO;
using CommandLine;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using StartupHouse.Database.Repository;

namespace StartupHouse.Database.Migrations
{
    class Program
    {
        /// <summary>
        ///     Class contains Application options.
        /// </summary>
        public class Options
        {
            /// <summary>
            ///     Name of the environment.
            /// </summary>
            [Option('e', "environment", Required = true, HelpText = "Set the Environment.")]
            public string Environment { get; set; }
        }

        /// <summary>
        ///     Main method.
        ///     Creates connection to DB based on provided <see cref="Options.Environment"/> parameter and performs DB migration.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {
                Parser.Default.ParseArguments<Options>(args)
                    .WithParsed<Options>(opts =>
                    {
                        var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json")
                            .AddJsonFile($"appsettings.{opts.Environment}.json", true, true);

                        var config = builder.Build();

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"ConnectionString: {config["ConnectionString"]}");
                        Console.ForegroundColor = ConsoleColor.White;

                        var dbContextOptionsBuilder = new DbContextOptionsBuilder<Context>();
                        dbContextOptionsBuilder
                            .EnableSensitiveDataLogging(true)
                            .UseSqlServer(config["ConnectionString"], providerOptions =>
                            {
                                providerOptions.CommandTimeout(60);
                                providerOptions.MigrationsAssembly("StartupHouse.Database.Migrations");
                            });

                        using (var context = new Context(dbContextOptionsBuilder.Options))
                        {
                            context.Database.Migrate();
                        }
                    });
            }
            catch (Exception ex)
            {
                var currentColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Exception: {ex.Message} {ex}");
                Console.WriteLine($"Inner Exception: {ex.InnerException}");
                Console.ForegroundColor = currentColor;
                throw;
            }
        }
    }
}