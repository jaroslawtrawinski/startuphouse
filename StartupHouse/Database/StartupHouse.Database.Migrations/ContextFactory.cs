﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using StartupHouse.Database.Repository;

namespace StartupHouse.Database.Migrations
{
    /// <summary>
    ///     Factory responsible for creating <see cref="Context"/>
    /// </summary>
    public class ContextFactory : IDesignTimeDbContextFactory<Context>
    {
        /// <summary>
        ///     Creates DbContext
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public Context CreateDbContext(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            var config = builder.Build();
            var dbContextOptionsBuilder = new DbContextOptionsBuilder<Context>();
            dbContextOptionsBuilder
                .UseSqlServer(config["ConnectionString"], providerOptions =>
                {
                    providerOptions.CommandTimeout(60);
                    providerOptions.MigrationsAssembly("StartupHouse.Database.Migrations");
                });

            return new Context(dbContextOptionsBuilder.Options);
        }
    }
}