﻿using System.Threading.Tasks;

namespace StartupHouse.Database.Interfaces
{
    /// <summary>
    ///     
    /// </summary>
    public interface IContextScope
    {
        /// <summary>
        ///     Commits changes to database.
        /// </summary>
        Task CommitAsync();
    }
}