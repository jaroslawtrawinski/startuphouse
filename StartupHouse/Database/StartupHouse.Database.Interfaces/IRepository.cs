﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace StartupHouse.Database.Interfaces
{
    /// <summary>
    ///     Interface describes base operations which can be done on repository.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity> where TEntity : IEntity
    {
        IQueryable<TEntity> Query { get; }

        IQueryable<TEntity> Queryable();

        TEntity Get(Expression<Func<TEntity, bool>> predicate);

        void Add(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);
    }
}