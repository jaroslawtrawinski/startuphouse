﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using StartupHouse.Interfaces.Exceptions;

namespace StartupHouse.Services.Api.Middleware
{
    /// <summary>
    ///     Middleware for handling exceptions.
    /// </summary>
    /// <remarks>
    ///     Exceptions being handled:
    ///     <para><see cref="EntityNotFoundException"/></para>
    ///     <para><see cref="RemoteApiException"/></para>
    ///     <para><see cref="InvalidOperationException"/></para>
    /// </remarks>
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILoggerFactory _loggerFactory;
        private ILogger Logger => _loggerFactory.CreateLogger("ExceptionMiddleware");

        /// <summary>
        ///     Initialize the instance.
        /// </summary>
        /// <param name="next">The function that can process an HTTP request.</param>
        /// <param name="loggerFactory">The logger.</param>
        public ExceptionMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            _loggerFactory = loggerFactory;
        }

        /// <summary>
        ///     Method invokes requested action.
        /// </summary>
        /// <param name="httpContext">The http context.</param>
        /// <returns></returns>
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (EntityNotFoundException ex)
            {
                await HandleEntityNotFoundExceptionAsync(httpContext, ex);
            }
            catch (RemoteApiException ex)
            {
                await HandleRemoteApiExceptionAsync(httpContext, ex);
            }
            catch (InvalidOperationException ex)
            {
                await HandleInvalidOperationExceptionAsync(httpContext, ex);
            }
            catch (Exception ex)
            {
                HandleException(httpContext, ex);
                throw;
            }
        }

        /// <summary>
        ///     Method handles <see cref="EntityNotFoundException"/>
        /// </summary>
        /// <param name="context">The Http context.</param>
        /// <param name="exception">The exception</param>
        private async Task HandleEntityNotFoundExceptionAsync(HttpContext context, EntityNotFoundException exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.NotFound;
            Logger.LogWarning(exception.Message);
            await context.Response.WriteAsync(exception.Message);
        }

        /// <summary>
        ///     Method handles <see cref="RemoteApiException"/>
        /// </summary>
        /// <param name="context">The Http context.</param>
        /// <param name="exception">The exception</param>
        private async Task HandleRemoteApiExceptionAsync(HttpContext context, RemoteApiException exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            Logger.LogWarning(exception.Message);
            await context.Response.WriteAsync(exception.Message);
        }

        /// <summary>
        ///     Method handles <see cref="InvalidOperationException"/>
        /// </summary>
        /// <param name="context">The Http context.</param>
        /// <param name="exception">The exception</param>
        private async Task HandleInvalidOperationExceptionAsync(HttpContext context, InvalidOperationException exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            Logger.LogWarning(exception.Message);
            await context.Response.WriteAsync(exception.Message);
        }

        /// <summary>
        ///     Method handles <see cref="Exception"/>
        /// </summary>
        /// <param name="context">The Http context.</param>
        /// <param name="exception">The exception</param>
        private void HandleException(HttpContext context, Exception exception)
        {
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            Logger.LogError(exception, exception.Message);
        }
    }
}
