﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StartupHouse.Interfaces;
using StartupHouse.Services.Api.ApiModels;

namespace StartupHouse.Services.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CurrenciesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICurrencyService _currencyService;

        /// <summary>
        ///     Initialize the instance.
        /// </summary>
        /// <param name="mapper">AutoMapper</param>
        /// <param name="currencyService">The currency service.</param>
        public CurrenciesController(
            IMapper mapper,
            ICurrencyService currencyService)
        {
            _mapper = mapper;
            _currencyService = currencyService;
        }

        /// <summary>
        ///     Gets list of supported currencies.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetSupportedCurrencies()
        {
            var dto = await _currencyService.GetCurrenciesAsync();
            var model = dto.Select(x => x.Symbol);
            return Ok(model);
        }

        /// <summary>
        ///     Gets average currency rate
        /// </summary>
        /// <param name="symbol">The symbol of the currency.</param>
        /// <param name="fromDate">Optional, default value: current day.</param>
        /// <param name="toDate">Optional, default value: current day</param>
        /// <returns></returns>
        [HttpGet("{symbol}")]
        public async Task<IActionResult> GetCurrencyData(string symbol, DateTime? fromDate, DateTime? toDate)
        {
            fromDate = fromDate?.Date ?? DateTime.Now.Date;
            toDate = toDate?.Date ?? DateTime.Now.Date;
            var dtoModel = await _currencyService.GetCurrencyDataAsync(symbol, fromDate.Value, toDate.Value);
            var apiModel = _mapper.Map<CurrencyApiModel>(dtoModel);
            return Ok(apiModel);
        }
    }
}