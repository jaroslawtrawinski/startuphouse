﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StartupHouse.Services.Api.ApiModels
{
    public class CurrencyApiModel
    {
        public string Symbol { get; set; }

        public decimal Avg => Data?.Values.Average() ?? 0M;

        public Dictionary<DateTime, decimal> Data { get; set; }
    }
}