﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using StartupHouse.Database.Interfaces;
using StartupHouse.Database.Repository;
using StartupHouse.Implementation;
using StartupHouse.Implementation.Remote;
using StartupHouse.Interfaces;
using StartupHouse.Interfaces.DTOs;
using StartupHouse.Interfaces.Remote;
using StartupHouse.Services.Api.ApiModels;
using StartupHouse.Services.Api.Middleware;

namespace StartupHouse.Services.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            #region Authentication

            services.AddAuthentication("Bearer")
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
                {
                    options.Authority = Configuration.GetValue<string>("Authentication:Authority");
                    options.RequireHttpsMetadata = Configuration.GetValue<bool>("Authentication:RequireHttpsMetadata");
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });

            services.AddCors(options =>
            {
                options.AddPolicy("global", builder =>
                {
                    builder.AllowAnyMethod();
                    builder.AllowAnyHeader();
                    builder.AllowAnyOrigin();
                    builder.AllowCredentials();
                });
            });
            
            #endregion

            #region Database

            var connectionString = Configuration.GetSection("ConnectionString").Get<string>();
            services.AddDbContext<Context>
            (options => options
                .UseLazyLoadingProxies()
                .UseSqlServer(connectionString));
            services.AddScoped<IContextScope, ContextScope>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<ICurrencyApi, NbpCurrencyApi>();
            #endregion

            #region Scoped services

            services.AddScoped(iServiceProvider =>
            {
                var mapper = GetAutoMapperConfiguration().CreateMapper();
                return mapper;
            });
            services.AddScoped<ICurrencyService, CurrencyService>();

            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseMiddleware<ExceptionMiddleware>();
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseMvc();
        }

        /// <summary>
        ///     Method gets AutoMapper mapping configuration.
        /// </summary>
        /// <returns></returns>
        public static MapperConfiguration GetAutoMapperConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CurrencyValuesDto, CurrencyApiModel>();
            });

            return config;
        }
    }
}
