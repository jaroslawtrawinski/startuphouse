﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using StartupHouse.Services.Auth.Security;

namespace StartupHouse.Services.Auth
{
    public class Startup
    {
        private readonly IHostingEnvironment _env;

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            _env = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            if (_env.IsDevelopment())
            {
                services.AddTransient<IResourceOwnerPasswordValidator, ResourceOwnerPasswordValidator>();
            }


            services.AddIdentityServer(options =>
                {
                    options.PublicOrigin = this.Configuration.GetValue<string>("Server:PublicOrigin");
                    options.IssuerUri = this.Configuration.GetValue<string>("Server:IssuerUri");
                    options.Discovery.ShowClaims = false;
                })
                .AddDeveloperSigningCredential()
                .AddInMemoryIdentityResources(new List<IdentityResource>
                {
                    new IdentityResources.OpenId(),
                    new IdentityResources.Profile()
                })
                .AddInMemoryApiResources(this.Configuration.GetSection("Resources"))
                .AddInMemoryClients(this.Configuration.GetSection("Clients"));

            services.AddAuthentication();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseIdentityServer();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
