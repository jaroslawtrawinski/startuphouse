﻿using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Validation;

namespace StartupHouse.Services.Auth.Security
{
    /// <summary>
    ///     User and Password custom validator.
    /// </summary>
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        /// <summary>
        ///     There should be user and password validations, but for demo purposes it accepts only user: Admin with Password: Admin
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            if (context.UserName == "Admin" && context.Password == "Admin")
            {
                context.Result = new GrantValidationResult(
                    subject: "Admin",
                    authenticationMethod: "custom"
                );
            }
            else
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Invalid username or password");
            }
        }
    }
}